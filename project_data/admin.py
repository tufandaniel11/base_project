from django.contrib import admin

from project_data.models import *

admin.site.register(ContactRequest)
admin.site.register(User)
