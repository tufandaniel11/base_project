from django import forms

from project_data.models import ContactRequest


class ContactRequestForm(forms.ModelForm):
    class Meta:
        model = ContactRequest
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for name, field in self.fields.items():
            if field.widget.__class__ == forms.widgets.Textarea or field.widget.__class__ == forms.widgets.TextInput:
                field.widget.attrs['rows'] = '3'
                field.widget.attrs['style'] = 'border-radius: 3px;'

            if 'class' in field.widget.attrs:
                field.widget.attrs['class'] += ' form-control input'
            else:
                field.widget.attrs.update({'class': 'form-control input'})