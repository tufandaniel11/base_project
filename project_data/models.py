from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone
from project_data.auth_backends import *


class User(AbstractUser):
    username = None
    email = models.EmailField(unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return f'{self.email}'

class ContactRequest(models.Model):
    name = models.CharField(max_length=50, default='')
    phone_number = models.CharField(max_length=30, default='')
    email = models.EmailField(default='')
    message_contact_request = models.TextField(max_length=1000, default='')
    attachment = models.FileField(upload_to='static/', null=True, blank=True)

    def __str__(self):
        return f'{self.name} tel:{self.phone_number} email:{self.email} msg:{self.message_contact_request[:25]}...'