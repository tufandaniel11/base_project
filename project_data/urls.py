from django.urls import path
from project_data import views

urlpatterns = [
    path('register/', views.register, name='register'),
    path('login/', views.user_login, name='login'),
    path('', views.ContactRequestCreateView.as_view(), name='home'),
    path('success', views.success_view, name='success'),
]