from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView

from project_data.forms import ContactRequestForm
from project_data.models import User, ContactRequest


def register(request):
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']
        if password == confirm_password:
            if User.objects.filter(email=email).exists():
                messages.error(request, 'Email is already registered.')
            else:
                user = User.objects.create_user(email=email, password=password)
                user.save()
                messages.success(request, 'Registration successful. You can now log in.')
                return redirect('login')
        else:
            messages.error(request, 'Passwords do not match.')
    return render(request, 'register.html')


def user_login(request):
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        user = authenticate(request, email=email, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, 'Login successful.')
            return redirect('')
        else:
            messages.error(request, 'Invalid email or password.')
    return render(request, 'login.html')


def success_view(request):
    return render(request, 'homepage.html', {'contact_form': ContactRequestForm(), 'success': True})


class ContactRequestCreateView(CreateView):
    template_name = 'homepage.html'
    success_url = reverse_lazy('success')
    model = ContactRequest
    form_class = ContactRequestForm
